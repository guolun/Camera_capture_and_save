#pragma once

/*李震
我的码云：https://git.oschina.net/git-lizhen
我的CSDN博客：http://blog.csdn.net/weixin_38215395
联系：QQ1039953685 */

#include <QThread>
#include <qdebug.h>

class Convert : public QThread
{
	Q_OBJECT

public:
	Convert();
	~Convert();

	void StartConvert();
	int input_w;
	int input_h;
	char *out_file_name;
	int framenumber;

protected:
	void run();
};
